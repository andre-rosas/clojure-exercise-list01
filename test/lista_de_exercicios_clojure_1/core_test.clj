(ns lista-de-exercicios-clojure-1.core-test
  
  (:require [clojure.test :refer :all]
            [lista-de-exercicios-clojure-1.core :as t]))

(def books [{:title "Pride and Prejudice" :author "Austen" :genre "romance" :copies-sold 15400}
            {:title "World War Z" :author "Brooks" :genre "zombie" :copies-sold 7250}
            {:title "Jurassic Park" :author "Crichton" :genre "sci-fi" :copies-sold 13677}
            {:title "The Lord of Rings" :author "Tolkien" :genre "fantasy" :copies-sold
             23450}
            {:title "Game of Thrones" :author "Martin" :genre "fantasy" :copies-sold
             19783}
            {:title "1984" :author "Orwell" :genre "sci-fi" :copies-sold 2600}])

(def vector-words ["André" "Rosas" "Rosas" "Rosas" "Luiz" "Luiz" "CSD" "CSD" "CSD" "CSD" "OK"])

;; 1 - Average-args
(deftest average-args-test
  (testing "[OK] when use a vector of integers."
    (is (= (t/average-args [5 5 10 27]) 11.75)))
  (testing "[OK] when use a vector of floats."
    (is (= (t/average-args [5.5 6.0 10.0 27.5]) 12.25)))
  (testing "[OK] when use a vector of mixed integers/floats."
    (is (= (t/average-args [5.5 6 10 27.5]) 12.25)))
  (testing "[OK] when use a vector of negatives."
    (is (= (t/average-args [-5.5 -6 -10 -27.5]) -12.25)))
  (testing "[OK] when use a vector of positive/negative, integers/float."
    (is (= (t/average-args [5.5 -6 -10 27.5]) 4.25)))
  (testing "[Fail] when use a list."
    (is (= (t/average-args '(5.5 5.8 10.0 27.5)) "It requires a vector of numbers!")))
  (testing "[Fail] when use a map."
    (is (= (t/average-args {:a 5.5 :b 6 :c 10 :d 27.5}) "It requires a vector of numbers!")))
  (testing "[Fail] when use a vector with string(s) inside it."
    (is (= (t/average-args ["5" 6 -10.0 27.5]) "One must not use strings inside a vector. Just numbers."))))

;; 2 - Scalar product between two vectors
(deftest scalar-product-vector-test
  (testing "[OK] when use a vector1 and vector2 of integers."
    (is (= (t/scalar-product-vector [0 1 2] [2 3 4]) 11)))
  (testing "[OK] when use a vector1 with positive integers and vector2 with negative integers."
    (is (= (t/scalar-product-vector [0 1 2] [-2 -3 -4]) -11)))
  (testing "[OK] when use a vector1 with positive floats and vector2 with negative integers."
    (is (= (t/scalar-product-vector [0 1.0 2.0] [-2 -3 -4]) -11.0)))
  (testing "[Fail] when use a vector1 with positive integers and vector2 with negative integers."
    (is (= (t/scalar-product-vector ["0" "1" "2"] [-2 -3 -4]) "One must not use strings inside a vector. Just numbers."))))

;; 3 - Fibonacci with LOOP
(deftest fibonacci-numbers-test
  (testing "[OK] when use a positive number above 2 as parameter."
    (is (= (t/fibonacci-numbers-vector-answer 8) [0 1 1 2 3 5 8 13])))
  (testing "[OK] when use a 1 as parameter."
    (is (= (t/fibonacci-numbers-vector-answer 1) [0])))
  (testing "[Fail] when use a 0 as parameter."
    (is (= (t/fibonacci-numbers-vector-answer 0) "You must use an integer above 0 as parameter!")))
  (testing "[Fail] when use a string as parameter."
    (is (= (t/fibonacci-numbers-vector-answer "8") "You must use an integer above 0 as parameter!")))
  (testing "[Fail] when use a string as parameter."
    (is (= (t/fibonacci-numbers-vector-answer 8.0) "You must use an integer above 0 as parameter!")))
  (testing "[OK] when use a positive number above 2 as parameter."
    (is (= (t/fibonacci-numbers-list-answer 8) '(0 1 1 2 3 5 8 13))))
  (testing "[OK] when use a 1 as parameter."
    (is (= (t/fibonacci-numbers-list-answer 1) '(0))))
  (testing "[Fail] when use a 0 as parameter."
    (is (= (t/fibonacci-numbers-list-answer 0) "You must use an integer above 0 as parameter!")))
  (testing "[Fail] when use a string as parameter."
    (is (= (t/fibonacci-numbers-list-answer "8") "You must use an integer above 0 as parameter!")))
  (testing "[Fail] when use a string as parameter."
    (is (= (t/fibonacci-numbers-list-answer 8.0) "You must use an integer above 0 as parameter!"))))

;; 4 - FizzBuzz Challenge
(deftest fizzbuzz-challange-test
  (testing "[OK] when everything all right from 1 to 100."
    (is (= (t/fizzbuzz-challenge-100) '("1"
                                        "2"
                                        "Fizz"
                                        "4"
                                        "Buzz"
                                        "Fizz"
                                        "7"
                                        "8"
                                        "Fizz"
                                        "Buzz"
                                        "11"
                                        "Fizz"
                                        "13"
                                        "14"
                                        "FizzBuzz"
                                        "16"
                                        "17"
                                        "Fizz"
                                        "19"
                                        "Buzz"
                                        "Fizz"
                                        "22"
                                        "23"
                                        "Fizz"
                                        "Buzz"
                                        "26"
                                        "Fizz"
                                        "28"
                                        "29"
                                        "FizzBuzz"
                                        "31"
                                        "32"
                                        "Fizz"
                                        "34"
                                        "Buzz"
                                        "Fizz"
                                        "37"
                                        "38"
                                        "Fizz"
                                        "Buzz"
                                        "41"
                                        "Fizz"
                                        "43"
                                        "44"
                                        "FizzBuzz"
                                        "46"
                                        "47"
                                        "Fizz"
                                        "49"
                                        "Buzz"
                                        "Fizz"
                                        "52"
                                        "53"
                                        "Fizz"
                                        "Buzz"
                                        "56"
                                        "Fizz"
                                        "58"
                                        "59"
                                        "FizzBuzz"
                                        "61"
                                        "62"
                                        "Fizz"
                                        "64"
                                        "Buzz"
                                        "Fizz"
                                        "67"
                                        "68"
                                        "Fizz"
                                        "Buzz"
                                        "71"
                                        "Fizz"
                                        "73"
                                        "74"
                                        "FizzBuzz"
                                        "76"
                                        "77"
                                        "Fizz"
                                        "79"
                                        "Buzz"
                                        "Fizz"
                                        "82"
                                        "83"
                                        "Fizz"
                                        "Buzz"
                                        "86"
                                        "Fizz"
                                        "88"
                                        "89"
                                        "FizzBuzz"
                                        "91"
                                        "92"
                                        "Fizz"
                                        "94"
                                        "Buzz"
                                        "Fizz"
                                        "97"
                                        "98"
                                        "Fizz"
                                        "Buzz"))))
  (testing "[OK] when range from 1 to 15, last element should be FizzBuzz."
    (is (= (t/fizzbuzz-challenge 15) '("1" "2" "Fizz" "4" "Buzz" "Fizz" "7" "8" "Fizz" "Buzz" "11" "Fizz" "13" "14" "FizzBuzz"))))
  (testing "[Fail] when use a float, like 15.0."
    (is (= (t/fizzbuzz-challenge 15.0) "You must use an integer as parameter!")))
  (testing "[Fail] when use a string, like '15'"
    (is (= (t/fizzbuzz-challenge "15") "You must use an integer as parameter!")))
  (testing "[Fail] when use a number below 0. Range starts from 1."
    (is (= (t/fizzbuzz-challenge 0) "You must use a integer above 0!")))
  (testing "[OK] when range starts in -15 and finish in 15. It should start and end with FizzBuzz."
    (is (= (t/fizzbuzz-challenge-master -15 15) '("FizzBuzz"
                                                  "-14"
                                                  "-13"
                                                  "Fizz"
                                                  "-11"
                                                  "Buzz"
                                                  "Fizz"
                                                  "-8"
                                                  "-7"
                                                  "Fizz"
                                                  "Buzz"
                                                  "-4"
                                                  "Fizz"
                                                  "-2"
                                                  "-1"
                                                  "FizzBuzz"
                                                  "1"
                                                  "2"
                                                  "Fizz"
                                                  "4"
                                                  "Buzz"
                                                  "Fizz"
                                                  "7"
                                                  "8"
                                                  "Fizz"
                                                  "Buzz"
                                                  "11"
                                                  "Fizz"
                                                  "13"
                                                  "14"
                                                  "FizzBuzz"))))
  (testing "[Fail] when se a float as parameter."
    (is (= (t/fizzbuzz-challenge-master 50.0 100) "You must use an integer as parameter!")))
  (testing "[Fail] when range start with number less than the number of the end of range."
    (is (= (t/fizzbuzz-challenge-master 200 100) "Integer of the First parameter should be less than the integer of the Second parameter."))))

;; 5 - Revert a sequence
(deftest reverse-sequence-test
  (testing "[OK] when use a string as parameter."
    (is (= (t/reverse-sequence "CSD") "DSC")))
  (testing "[OK] when use a list as parameter."
    (is (= (t/reverse-sequence '(1 2 3 4)) '(4 3 2 1))))
  (testing "[OK] when use a vector as parameter."
    (is (= (t/reverse-sequence [1 2 3 4]) '(4 3 2 1))))
  (testing "[OK] when use a map as parameter."
    (is (= (t/reverse-sequence {:a 1 :b 2}) '([:b 2] [:a 1]))))
  (testing "[Fail] when use a nil as parameter."
    (is (= (t/reverse-sequence nil) "You've missed the parameter(s) or used a wrong one(s). Parameter(s) is/are required.")))
  (testing "[OK] when use a string as parameter."
    (is (= (t/reverse-sequence-with-restriction "CSD") "DSC")))
  (testing "[OK] when use a list as parameter."
    (is (= (t/reverse-sequence-with-restriction '(1 2 3 4)) '(4 3 2 1))))
  (testing "[OK] when use a vector as parameter."
    (is (= (t/reverse-sequence-with-restriction [1 2 3 4]) '(4 3 2 1))))
  (testing "[OK] when use a map as parameter."
    (is (= (t/reverse-sequence-with-restriction {:a 1 :b 2}) '([:b 2] [:a 1]))))
  (testing "[Fail] when use a nil as parameter."
    (is (= (t/reverse-sequence-with-restriction nil) "You've missed the parameter(s) or used a wrong one(s). Parameter(s) is/are required."))))

;; 6 - Get total of books sold
(deftest total-copies-sold-test
  (testing "[OK] when use a previously def books as parameter."
    (is (= (t/total-copies-sold books) 82160)))
  (testing "[Fail] when not use a previously def books as parameter."
    (is (= (t/total-copies-sold []) 0))))

;; 7 - Three more frequent words
(deftest get-three-more-frequent-words-test
  (testing "[OK] when returns the three more frequent words."
    (is (= (t/get-three-more-frequent-words vector-words) '("CSD" "Rosas" "Luiz"))))
  (testing "[Fail] when you need to use three or more words."
    (is (= (t/get-three-more-frequent-words ["Error" "Fail"]) "You need more words!")))
  (testing "[Fail] when you need to use a vector of strings."
    (is (= (t/get-three-more-frequent-words "André") "Parameter needs to be a vector of strings.")))
  (testing "[Fail] when you need to use a vector of strings."
    (is (= (t/get-three-more-frequent-words '("André" "CSD")) "You need to use a vector with at least 3 strings!"))))

;; 8 - Three biggest words
(deftest get-three-biggest-words-test
  (testing "[OK] when returns the three biggest words by length of words."
    (is (= (t/get-three-biggest-words vector-words) '([5 ["André" "Rosas" "Rosas" "Rosas"]] [4 ["Luiz" "Luiz"]] [3 ["CSD" "CSD" "CSD" "CSD"]]))))
  (testing "[Fail] when you need to use three or more words."
    (is (= (t/get-three-biggest-words ["Error" "Fail"]) "You need more words!")))
  (testing "[Fail] when you need to use a vector of strings."
    (is (= (t/get-three-biggest-words "André") "Parameter needs to be a vector of strings.")))
  (testing "[Fail] when you need to use a vector of strings."
    (is (= (t/get-three-biggest-words '("André" "CSD")) "You need to use a vector with at least 3 strings!"))))

;; 9 - Bigger than x and divisible by p
(deftest first-n-bigger-than-x-and-divisible-by-p-test
  (testing "[OK] when returns 4 numbers bigger than 20 which is divisible by 2."
    (is (= (t/first-n-bigger-than-x-and-divisible-by-p 4 20 2) '(22 24 26 28))))
  (testing "[OK] when returns 3 numbers bigger than 10 which is divisible by 3."
    (is (= (t/first-n-bigger-than-x-and-divisible-by-p 3 10 3) '(12 15 18))))
  (testing "[OK] when returns 1 number bigger than 1 which is divisible by 1."
    (is (= (t/first-n-bigger-than-x-and-divisible-by-p 1 1 1) '(2))))
  (testing "[Fail] when one or more parameters is below 1."
    (is (= (t/first-n-bigger-than-x-and-divisible-by-p 4 20 0) "Parameters should be integers and integer above 0.")))
  (testing "[Fail] when every parameters is below 1."
    (is (= (t/first-n-bigger-than-x-and-divisible-by-p 0 0 0) "Parameters should be integers and integer above 0.")))
  (testing "[Fail] when one or more parameter is negative."
    (is (= (t/first-n-bigger-than-x-and-divisible-by-p -5 20 2) "Parameters should be integers and integer above 0.")))
  (testing "[Fail] when one or more parameter is a string (or not integer)."
    (is (= (t/first-n-bigger-than-x-and-divisible-by-p "5" 20 2) "Parameters should be integers and integer above 0.")))
  (testing "[Fail] when one or more parameter is a float (or not integer)."
    (is (= (t/first-n-bigger-than-x-and-divisible-by-p 5.0 20 2) "Parameters should be integers and integer above 0.")))
  )

;; 10 - Goldbach conjecture
(deftest goldbach-conjecture-test
  (testing "[OK] when used a valid positive integer and even number above 2."
    (is (= (t/goldbach-conjecture-max-pair 28) '(5 23))))
  (testing "[Fail] when used a 27. Invalid number because 27 is an odd number."
    (is (= (t/goldbach-conjecture-max-pair 27) "Number is not possible for Goldbach conjecture in this function. Use another number.")))
  (testing "[Fail] when used a 28.0. Invalid number because 28.0 is a float number."
    (is (= (t/goldbach-conjecture-max-pair 28.0) "Number is not possible for Goldbach conjecture in this function. Use another number.")))
  (testing "[Fail] when used a 0. Invalid number."
    (is (= (t/goldbach-conjecture-max-pair 0) "Number is not possible for Goldbach conjecture in this function. Use another number.")))
  (testing "[Fail] when used a 2. Invalid number."
    (is (= (t/goldbach-conjecture-max-pair 2) "Number is not possible for Goldbach conjecture in this function. Use another number.")))
  (testing "[Fail] when used a -2. Invalid number."
    (is (= (t/goldbach-conjecture-max-pair -2) "Number is not possible for Goldbach conjecture in this function. Use another number.")))
  (testing "[Fail] when used a 1900. The limit is 1000."
    (is (= (t/goldbach-conjecture-max-pair 1900) "The maximum number allowed is 1000."))))

;;end-of-file