(ns lista-de-exercicios-clojure-1.core

(:gen-class)
(:require [clojure.spec.gen.alpha :refer [string]]))

;; 1 - Average numbers
(defn average-args
  "Returns average number from a vector of numbers."
  [args]
  (if (some string? args)
    "One must not use strings inside a vector. Just numbers."
    (if (vector? args)
      (float(/ (reduce + args) (count args)))
      "It requires a vector of numbers!")))

;; 2 - Scalar product between two vectors
(defn scalar-product-vector
  "Returns the sum of multiplication of two vectors.
  v1 * v2 = SUM (v1_i + v2_i) = v1_1*v2_1 + v1_2*v2_2 + ... + v1_n*v2_n"
  [vector1 vector2]
  (cond
    (or (some string? vector1) (some string? vector2)) "One must not use strings inside a vector. Just numbers."
    :else (reduce + (mapv * vector1 vector2))))

;; 3 - Fibonacci with LOOP
(defn fibonacci-numbers-vector-answer
  "Return first numbers of a Fibonacci sequence set by 'number' in a vector structure."
  [number]
  (if (and (integer? number) (> number 0))
    (loop [fib-numbers [0 1]]
      (if (>= (count fib-numbers) number)
        (subvec fib-numbers 0 number)
        (let [[number1 number2] (reverse fib-numbers)]
          (recur (conj fib-numbers (+ number1 number2))))))
    "You must use an integer above 0 as parameter!"))

(defn fibonacci-numbers-list-answer
  "Return first numbers of a Fibonacci sequence set by 'number' in a list structure."
  [number]
  (if (and (integer? number) (> number 0))
    (loop [fib-number (- number 2) result '(0 1)]
      (if (>= fib-number 0)
        (recur (dec fib-number)
               (conj result
                     (reduce + (take 2 result))))
        (drop 1 (reverse result))))
    "You must use an integer above 0 as parameter!"))

;; 4 - FizzBuzz Challenge
(defn fizzbuzz-challenge-100
  "Returns FizzBuzz if number in a range from 1 to 100 is divisible simultaneously by 3 and 5 in a structure of a list.
  Returns Fizz if number in a range from 1 to 100 is divisible only by 3 in a structure of a list.
  Returns Buzz if number in a range from 1 to 100 is divisible only by 5 in a structure of a list.
  Returns str(number) if number in a range from 1 to 100 is not divisible by 3 and/or 5 in a structure of a list."
  []
  (map (fn [item]
       (cond
         (zero? (mod item (* 3 5))) "FizzBuzz"
         (zero? (mod item 3)) "Fizz"
         (zero? (mod item 5)) "Buzz"
         :else (str item)))
       (range 1 101)))

(defn fizzbuzz-challenge
  "Returns FizzBuzz if number in a range from 1 to any integer positive number is divisible simultaneously by 3 and 5 in a structure of a list.
  Returns Fizz if number in a range from 1 to any integer positive number is divisible only by 3 in a structure of a list.
  Returns Buzz if number in a range from 1 to any integer positive number is divisible only by 5 in a structure of a list.
  Returns str(number) if number in a range from 1 to any positive integer positive is not divisible by 3 and/or 5 in a structure of a list."
  [number-to-end]
  (if (string? number-to-end)
    "You must use an integer as parameter!"
    (if (< number-to-end 1)
      "You must use a integer above 0!"
      (if (integer? number-to-end)
        (map (fn [item]
             (cond
               (zero? (mod item (* 3 5))) "FizzBuzz"
               (zero? (mod item 3)) "Fizz"
               (zero? (mod item 5)) "Buzz"
               :else (str item)))
             (range 1 (inc number-to-end)))
        "You must use an integer as parameter!"))))

(defn fizzbuzz-challenge-master
  "Returns FizzBuzz if number in a range of any integer number is divisible simultaneously by 3 and 5 in a structure of a list.
  Returns Fizz if number in a range of any integer number is divisible only by 3 in a structure of a list.
  Returns Buzz if number in a range of any integer number is divisible only by 5 in a structure of a list.
  Returns str(number) if number in a range of any integer number is not divisible by 3 and/or 5 in a structure of a list."
  [number-to-start number-to-end]
  (if (or (string? number-to-start) (string? number-to-end))
    "You must use an integer as parameter!"
    (if (< number-to-end number-to-start)
      "Integer of the First parameter should be less than the integer of the Second parameter."
      (if (and (integer? number-to-start) (integer? number-to-end))
        (map (fn [item]
               (cond
                 (zero? (mod item (* 3 5))) "FizzBuzz"
                 (zero? (mod item 3)) "Fizz"
                 (zero? (mod item 5)) "Buzz"
                 :else (str item)))
             (range number-to-start (inc number-to-end)))
        "You must use an integer as parameter!"))))

;; 5 - Revert a sequence
(defn reverse-sequence
  "Returns a list with a sequence reverted."
  [args]
  (if (string? args)
    (apply str (reverse args))
    (if (map? args)
      (rseq (into (sorted-map) args))
      (if (list? args)
        (reverse args)
        (if (vector? args)
          (rseq args)
          "You've missed the parameter(s) or used a wrong one(s). Parameter(s) is/are required.")))))

(defn reverse-sequence-with-restriction
  "Returns a list with a sequence reverted Without reverse and rseq."
  [args]
  (if (string? args)
    (apply str (reduce conj () args))
    (if (map? args)
      (reduce conj () (into (sorted-map) args))
      (if (list? args)
        (reduce conj () args)
        (if (vector? args)
          (reduce conj () args)
          "You've missed the parameter(s) or used a wrong one(s). Parameter(s) is/are required.")))))

;; 6 - Get total of books sold
(def books [{:title "Pride and Prejudice" :author "Austen" :genre "romance" :copies-sold 15400}
            {:title "World War Z" :author "Brooks" :genre "zombie" :copies-sold 7250}
            {:title "Jurassic Park" :author "Crichton" :genre "sci-fi" :copies-sold 13677}
            {:title "The Lord of Rings" :author "Tolkien" :genre "fantasy" :copies-sold
             23450}
            {:title "Game of Thrones" :author "Martin" :genre "fantasy" :copies-sold
             19783}
            {:title "1984" :author "Orwell" :genre "sci-fi" :copies-sold 2600}])

(defn total-copies-sold
  "Returns the total of copies sold in books (previously defined)."
  [books]
  (reduce + (map :copies-sold books)))

;; 7 - Three more frequent words
(def vector-words ["André" "Rosas" "Rosas" "Rosas" "Luiz" "Luiz" "CSD" "CSD" "CSD" "CSD" "OK"])

(defn get-three-more-frequent-words
  "Returns a list with most frequent words."
  [args]
  (cond
    (and (vector? args) (>= (count args) 3)) (take 3 (reverse (keys (into (sort-by val (frequencies args))))))
    (and (vector? args) (< (count args) 3) )"You need more words!"
    (and (not= args vector) (>= (count args) 3)) "Parameter needs to be a vector of strings."
    :else "You need to use a vector with at least 3 strings!"))

;; 8 - Three biggest words
(defn get-three-biggest-words
  "Returns a list with three biggest words."
  [args]
  (cond
    (and (vector? args) (>= (count args) 3)) (take 3 (group-by count args))
    (and (vector? args) (< (count args) 3) )"You need more words!"
    (and (not= args vector) (>= (count args) 3)) "Parameter needs to be a vector of strings."
    :else "You need to use a vector with at least 3 strings!"))

;; 9 - Bigger than x and divisible by p
(defn first-n-bigger-than-x-and-divisible-by-p
  "Returns the first (n) numbers bigger than (x) and divisible py (p)."
  [n x p]
  (if (and (and (integer? n) (> n 0)) (and (integer? x) (> x 0)) (and (integer? p) (> p 0)))
    (take n (filter #(zero? (mod % p)) (range (inc x) (* n x p 100))))
    "Parameters should be integers and integer above 0."))

;; 10 - Goldbach conjecture
(defn prime-number?
  "Returns if a number is prime or not."
  [number]
  (cond
    (not (number? number)) (throw (IllegalArgumentException. "It requires an integer number!"))
    (< number 2) false
    :else
    (empty? (filter #(= 0 %) (map #(mod number %) (range 2 number))))))

(defn goldbach-conjecture
  "Returns the finding of two prime numbers that sum an even integer lower than 1000."
  [number]
  (if (> number 1000)
    "The maximum number allowed is 1000."
    (map #(sort (rest %))
       (filter #(= (first %) true)
               (for [n1 (range 0 (inc number)) n2 (range 0 number)]
                 (list (and (= (+ n1 n2) number)
                            (prime-number? n1)
                            (prime-number? n2))
                             n1 n2))))))

(defn goldbach-conjecture-max-pair
  "Find the pair where the difference between the first and second number is maxed."
  [number]
  (if (> number 1000)
    "The maximum number allowed is 1000."
    (if (empty? (goldbach-conjecture number))
      "Number is not possible for Goldbach conjecture in this function. Use another number."
      (next (first (sort-by first > (map #(let [fst (first %) scd (second %)]
                                            (list (- scd fst) fst scd)) (goldbach-conjecture number))))))))

(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))